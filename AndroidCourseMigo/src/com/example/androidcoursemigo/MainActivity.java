package com.example.androidcoursemigo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;


import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.os.Build;


public class MainActivity extends ActionBarActivity {
	StringBuilder sb = new StringBuilder();
	int itemcount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);
        
        final TextView output = (TextView) findViewById(R.id.TextView04);
		StringBuffer sb = new StringBuffer();
		final Spinner spin = (Spinner) findViewById(R.id.spinner1);
		ArrayList<String> list = new ArrayList<String>();
		ArrayAdapter<String> adapter;

		adapter = new ArrayAdapter<String>(this, R.layout.spinner_city, list);

		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(getAssets().open(
					"map.json")));
			String temp;
			while ((temp = br.readLine()) != null) {
				sb.append(temp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		String myJSONstring = sb.toString();
		try {
			JSONObject jsnregion = new JSONObject(myJSONstring);
			JSONArray jsnArr = jsnregion.getJSONArray("region");
			JSONArray jsnCity;

			for (int i = 0; i < jsnArr.length(); i++) {
				JSONObject jsnObj = jsnArr.getJSONObject(i);
				String name = jsnObj.getString("name");
				JSONArray jsnArrreg = jsnObj.getJSONArray("cityInfo");
				for (int x = 0; x < jsnArrreg.length(); x++) {
					JSONObject jsnObjreg = jsnArrreg.getJSONObject(x);
					list.add(jsnObjreg.getString("name"));
					adapter.notifyDataSetChanged();
					itemcount++;
				}
			}

			spin.setAdapter(adapter);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			Button buttonnxt = (Button) findViewById(R.id.Button02);
			buttonnxt.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					int ctr = spin.getSelectedItemPosition() + 1;
					try {
						if (ctr == itemcount) {
							spin.setSelection(0);
						} else {
							spin.setSelection(ctr);
						}

					} catch (Exception e) {

					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
}
